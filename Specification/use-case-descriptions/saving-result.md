# Saving the final result

**Description**: After the game is over, application stores the game results.

**Actors**:
-Players: Players finished the game.

**Preconditions**:
-Application is running and the player successfully finished the game.

**Postconditions**: Result and statistics of the game are saved.

**Main scenario**:
1. Application stores results
2. Application creates dialog with results
3. Dialog is displayed to the player
4. Player closes dialog with results
5. Dialog closes
6. Game closes


**Alternative scenarios**:
-A1: **Unexpected application exit**: Application closes and results are not saved

**Subscenarios**: None.

**Special requirements**: None.

**Additional information**: None.
