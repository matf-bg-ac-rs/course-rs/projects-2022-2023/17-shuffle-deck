# Displaying How to play window

**Description**: The application displays How to play window with game rule description. Player has an option to return to the main menu.

**Actors**:
-Player: chooses an option from the main menu

**Preconditions**:
-Application is running and the main menu is opened.

**Postconditions**: 
-Window with game rules is shown

**Main scenario**:
1. Player chooses a button "How to play" from main menu
2. Application opens How to play window and displays game rule description.
3. Player chooses a button "Return" and then returns to the main menu.

**Alternative scenarios**:
-A1: **Unexpected application exit**: The selection is discarded and the application closes

**Subscenarios**: None.

**Special requirements**: None.

**Additional information**: None.
