# Displaying Settings

**Description**: The application displays Settings window with options to mute the sound and change window to full screen. 
Player has an option to return to the main menu.

**Actors**:
-Player: chooses an option from the main menu

**Preconditions**:
-Application is running and the main menu is opened.

**Postconditions**: 
-PLayer set up game settings.

**Main scenario**:
1. Player chooses a button "Settings" from main menu
2. Application opens Settings window and displays possible options.
    1. If player un/checks "Mute" trigger then the game sound should start/stop playing
    2. If player un/checks "Full Screen" trigger then the game window should go to full screen/windowed mode
4. Player chooses a button "Return" and then returns to the main menu.

**Alternative scenarios**:
-A1: **Unexpected application exit**: Settings are discarded and the application closes

**Subscenarios**: None.

**Special requirements**: None.

**Additional information**: None.
