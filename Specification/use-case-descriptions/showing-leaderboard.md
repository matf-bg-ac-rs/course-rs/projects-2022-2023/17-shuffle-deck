# Displaying leaderboard

**Description**: The application displays leaderboard. After displaying it, the player has an option to return to the main menu.

**Actors**:
-Player: chooses an option from the main menu

**Preconditions**:
-Application is running and the main menu is opened.

**Postconditions**: 
-Ten best results are shown.

**Main scenario**:
1. Player chooses a button "Leaderboard" from main menu
2. Application opens leaderboard window and displays a list of ten best results
3. Player chooses a button "Return" and then returns to the main menu.

**Alternative scenarios**:
-A1: **Unexpected application exit**: The selection is discarded and the application closes

**Subscenarios**: None.

**Special requirements**: None.

**Additional information**: None.
