# Online Game

**Description**: blackjack, or twenty-one, Card game whose object is to be dealt cards having a higher count than those of the dealer, up to but not exceeding 21. The dealer may use a single deck of 52 cards or two or more decks from a holder called a shoe. Aces count as 1 or 11, and face cards as 10. Depending on the rules used, bets may be placed before the deal, after each player has been dealt one card facedown, or after each player has received two cards facedown and the dealer has exposed one of his cards.

**Actors**:
- The Player: initiates the game and plays online game with other players and Bot "Dealer"
- Dealer: AI Bot which deal cards and control game

**Preconditions**: 
- "Game settings" use case
- "How to play" use case
- "Training" use case

**Postconditions**: Player quit online game or lose all of his money

**Main scenario**:
1. Player chooses Online mode  
2. Application asks Player to input amout of entry money
3. Player enters amount of money
4. App Initialize an instance of Game
5. Dealer shuffles a deck
6. Player bets amout of money for the turn and his balance is decreased by that amount
7. Dealer deals 2 cards to Player
8. Dealer deals 2 cards to Bots
9. Dealer deals 2 cards to himself
10. While Player has money or Player doesn't exit the game:
    1. Game determines whos turn is it
    2. If it's Players turn then:
        1. While Player doesn't choose "Stay" and his score is <= 21
            1. Playar can choose "Hit"
            2. Dealer deals 1 card to Player
        2. If Player chooses "Stay"
            1. Players turn ends
        3. If Player's score is > 21
            1. Game notifies Player he lost and ends his turn
    3. If it's not Players turn
        1. Wait for other bot Players to end their turn
        2. Wait for Dealers turn
        3. While the Dealer's score is < 17
            1. Dealer "Hit" and deals 1 card to himslef
        4. If Players scores (including bot players) are > Dealer's score
            1. Players win and their money balance is increased by double the bet amount
        5. If Players scores (including bot players) are = Dealer's score
            1. Players draw and their money balance is increased by the bet amount
    4. If deck is empty
        1. Game makes new deck and shuffle it
11. At the end Game shows amount of money won during the game


**Alternative scenarios**:
- A1: **Unexpected application exit**: The online game is discarded and the application closes.

**Subscenarios**: None.

**Special requirements**: None.

**Additional information**: None.
